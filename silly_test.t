use Test::More tests => 120;

use strict;
use warnings;

use ok 'Test::Exception';
SKIP: {
    skip('too lazy to fix', 1);
    ok(0);
}

for (1 .. 100) {
    ok(1, "$_ is ok");
}

TODO: {
    local $TODO = 'hi';
    ok(0, 'nope');
    ok(0, 'nope');
    ok(0, 'nope');
}

ok(1, 'yep');
ok(1, 'yep');
ok(1, 'yep');
ok(1, 'yep');

ok(0, 'nope');
ok(0, 'nope');
ok(0, 'nope');
ok(0, 'nope');
ok(0, 'nope');

ok(1, 'yep');

