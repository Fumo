-- MySQL dump 10.11
--
-- Host: ops-dbdev1.national.core    Database: fumo
-- ------------------------------------------------------
-- Server version	5.0.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `branch`
--

DROP TABLE IF EXISTS `branch`;
CREATE TABLE `branch` (
  `name` varchar(128) NOT NULL,
  `project` varchar(128) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY  (`name`,`project`),
  KEY `fk_project` (`project`),
  CONSTRAINT `fk_branch_project` FOREIGN KEY (`project`) REFERENCES `project` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branch`
--

LOCK TABLES `branch` WRITE;
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `name` varchar(255) NOT NULL,
  `project` varchar(128) NOT NULL,
  `branch` varchar(128) NOT NULL,
  `revision` varchar(128) NOT NULL,
  `test_count` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `pass_rate` float NOT NULL default '0',
  PRIMARY KEY  (`name`,`project`,`branch`,`revision`),
  KEY `fk_file_revision` (`revision`),
  KEY `fk_file_project` (`project`),
  KEY `fk_file_branch` (`branch`),
  CONSTRAINT `fk_file_branch` FOREIGN KEY (`branch`) REFERENCES `branch` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_file_project` FOREIGN KEY (`project`) REFERENCES `project` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_file_revision` FOREIGN KEY (`revision`) REFERENCES `revision` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `name` varchar(128) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY  (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
CREATE TABLE `queue` (
  `project` varchar(128) NOT NULL,
  `branch` varchar(128) NOT NULL,
  `revision` varchar(128) NOT NULL,
  `created` datetime NOT NULL,
  `run_start` datetime default NULL,
  `run_finish` datetime default NULL,
  `weight` int(11) NOT NULL default '0',
  `status` enum('complete','running','queued','invalid') NOT NULL default 'queued',
  `comment` text NOT NULL,
  PRIMARY KEY  (`project`,`branch`,`revision`),
  KEY `fk_queue_branch` (`branch`),
  KEY `fk_queue_revision` (`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `queue`
--

LOCK TABLES `queue` WRITE;
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revision`
--

DROP TABLE IF EXISTS `revision`;
CREATE TABLE `revision` (
  `name` varchar(128) NOT NULL,
  `project` varchar(128) NOT NULL,
  `branch` varchar(128) NOT NULL,
  `created` datetime NOT NULL,
  `pass_rate` int(3) NOT NULL default '0',
  PRIMARY KEY  (`name`,`project`,`branch`),
  KEY `fk_revision_branch` (`branch`),
  KEY `fk_revision_project` (`project`),
  CONSTRAINT `fk_revision_branch` FOREIGN KEY (`branch`) REFERENCES `branch` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_revision_project` FOREIGN KEY (`project`) REFERENCES `project` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `revision`
--

LOCK TABLES `revision` WRITE;
/*!40000 ALTER TABLE `revision` DISABLE KEYS */;
/*!40000 ALTER TABLE `revision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `name` varchar(255) NOT NULL,
  `project` varchar(128) NOT NULL,
  `branch` varchar(128) NOT NULL,
  `revision` varchar(128) NOT NULL,
  `file` varchar(255) NOT NULL,
  `number` int(11) NOT NULL,
  `status` enum('ok','not ok') default NULL,
  `created` datetime NOT NULL,
  `directive` enum('TODO','SKIP') default NULL,
  `pass_rate` int(3) NOT NULL,
  PRIMARY KEY  USING BTREE (`name`,`project`,`branch`,`revision`,`file`,`number`),
  KEY `fk_test_file` (`file`),
  KEY `fk_test_branch` (`branch`),
  KEY `fk_test_revision` (`revision`),
  KEY `fk_test_project` (`project`),
  CONSTRAINT `fk_test_branch` FOREIGN KEY (`branch`) REFERENCES `branch` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_test_file` FOREIGN KEY (`file`) REFERENCES `file` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_test_project` FOREIGN KEY (`project`) REFERENCES `project` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_test_revision` FOREIGN KEY (`revision`) REFERENCES `revision` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2007-07-26 16:06:33
