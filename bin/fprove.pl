#!/usr/bin/env perl

use strict;
use warnings;

use TAPx::Parser;
use TAPx::Parser::Aggregator;

use LWP::UserAgent;
use YAML qw(Dump);
use Data::Dumper;

use Getopt::Long;

use File::Spec;
use File::Find;

sub post_to_fumo {
    my ($fumo_url, $struct) = @_;

    my $ua = LWP::UserAgent->new();
    $ua->agent($0);

    # build up the request, data will be yaml
    my $yaml = Dump $struct;
    my $request = HTTP::Request->new(POST => $fumo_url);
    $request->content_type('text/x-yaml');
    $request->content($yaml);

    # how did we do
    my $response = $ua->request($request);

    if (not $response->is_success()) {
        warn $response->as_string();

        my $msg = "Posting to Fumo failed, the YAML that was sent "
            . "was written to 'fumo.out'";
        warn $msg;

        YAML::DumpFile('fumo.out', $yaml);
    }
}

sub middle_truncate {
    my ($string, $size, $replacement) = @_;

    return $string if length $string <= $size;

    my $half = int (($size - length($replacement)) / 2);
    $string =~ s/^(.{$half}).*(.{$half})$/$1$replacement$2/g;

    return $string;
}

sub get_files {
    my ($file, $recurse) = @_;
    my @files;

    if ($recurse and -d $file) {
        find(
            sub { -f && /\.t$/ && push @files => $File::Find::name },
            $file
        );
    }
    elsif (-d $file) {
        @files = glob(File::Spec->catfile($file, '*.t' ));
    }
    elsif (-f $file) {
        push @files, $file;
    }

    return @files;
}

MAIN: {
    my $test_name_len = 255;
    my $file_name_len = 255;

    GetOptions(
        'l|lib'           => \my $lib,
        'f|fumo-url=s'    => \my $fumo_url,
        'r|recurse'       => \my $recurse,
        't|test-name-len' => \$test_name_len,
        'i|file-name-len' => \$file_name_len,
    );

    my @files = map { get_files($_, $recurse) } @ARGV;
    my $struct = {
        files => [ ]
    };

    # args for tap-parser
    my $args = { };
    if ($lib) {
        $args->{switches} = "-I$lib";
    }

    my $aggr_all = TAPx::Parser::Aggregator->new;
    my $all_tests_planned = 0;

    # execute each of the files given to us on the command line
    for my $file (@files) {
        my $parser = TAPx::Parser->new({
            source => $file,
        });

        my (@tests, $file_tests_planned);
        while (my $item = $parser->next) {
            # we need to capture the actual test details
            if (ref $item eq 'TAPx::Parser::Results::Test') {
                my $desc = ($item->description)
                    ? $item->number . ' ' . $item->description
                    : 'fumo: no description for test ' . $item->number;

                push @tests, {
                    number    => $item->number,
                    status    => $item->ok,
                    name      => middle_truncate($desc, $test_name_len, '...'),
                    directive => $item->directive,
                };
            }
            elsif (ref $item eq 'TAPx::Parser::Results::Plan') {
                $file_tests_planned = ($item->tests_planned || 0);
                $all_tests_planned += $file_tests_planned;
            }

            # this gives us much the same output as prove
            print $item->as_string() . "\n";
        }

        $aggr_all->add($file, $parser);

        # no tests = no logging
        if (scalar @tests) {
            push @{$struct->{files}}, {
                name       => middle_truncate($file, $file_name_len, '...'),
                test_count => $file_tests_planned,
                tests      => \@tests,
            };
        }
    }

    my $perc = ($all_tests_planned)
        ? $aggr_all->passed() * 100 / $all_tests_planned
        : 0;

     warn sprintf "\nplanned: %d, ran: %d, passed: %d, failed: %d\n",
         ($all_tests_planned or 0), (scalar $aggr_all->total() or 0),
         ($aggr_all->passed() or 0), ($aggr_all->failed() or 0);

     warn sprintf "skipped: %d, todo'd: %d, rate: %.2f%%\n",
         ($aggr_all->skipped() or 0), ($aggr_all->todo() or 0), $perc;

    # do we want to post to fumo?
    if ($fumo_url) {
        post_to_fumo($fumo_url, $struct);
    }

    exit $aggr_all->failed() ? 1 : 0;  # error status if any test failed
}
