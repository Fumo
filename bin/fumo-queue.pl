#!/usr/bin/env perl

use strict;
use warnings;

use DBI;

use Fumo::Queue;
use Fumo::Queue::Run;

use Term::ReadLine;
use Getopt::Long;

use Data::Dumper;

sub _assert_args {
    my $min_count = shift;
    my $arg_count = scalar @_;

    if ($arg_count < $min_count) {
        die "Not enough arguments provided (need $min_count)\n";
    }

    return @_;
}

sub list {
    my ($queue, $out) = @_[0,1];
    my @ARGV  = _assert_args(0, @_);

    while (my $run = $queue->all_runs()) {
        printf $out "%s,%s,%s\n", $run->project, $run->branch, $run->revision;
    }
}

sub add {
    my $queue = shift;
    my $out   = shift;
    my ($project, $branch, $revision) = _assert_args(3, @_);

    if (my $run = $queue->get_run($project, $branch, $revision)) {
        print $out "$project, $branch, $revision alread in queue.";
    }
    else {
        my $run = Fumo::Queue::Run->new({
            project  => $project,
            branch   => $branch,
            revision => $revision
        });

        $queue->put_run($run);
    }
}

sub up {
    my $queue = shift;
    my $out   = shift;
    my ($project, $branch, $revision, $amount) = _assert_args(3, @_);

    if (my $run = $queue->get_run($project, $branch, $revision)) {
        $run->weight($run->weight() + ($amount or 10));
        $queue->put_run($run);
    }
    else {
        print $out "$project, $branch, $revision doesn't exist (try list).";
    }
}

sub down {
    my $queue = shift;
    my $out   = shift;
    my ($project, $branch, $revision, $amount) = _assert_args(3, @_);

    $amount = -$amount if defined $amount;

    up($queue, $out, $project, $branch, $revision, ($amount or -10));
}

sub delete {
    my $queue = shift;
    my $out   = shift;
    my ($project, $branch, $revision) = _assert_args(3, @_);

    if (my $run = $queue->get_run($project, $branch, $revision)) {
        $queue->delete($run);
    }
    else {
        print $out "$project, $branch, $revision doesn't exist (try list).";
    }
}

MAIN: {
    my ($host, $port, $password, $username, $database) 
        = ('localhost', 3307, '', $ENV{USER}, 'fumo');

    GetOptions(
        'host=s'     => \$host,
        'port=i'     => \$port,
        'password=s' => \$password,
        'username=s' => \$username,
        'database=s' => \$database,
        'stdin'      => \my $stdin
    ) or die "try --help for help\n";

    my $dbh = DBI->connect("dbi:mysql:$database:host=$host:port=$port",
                           "$username", "$password");

    my $queue = Fumo::Queue->new($dbh);

    # TODO: detect this...
    my ($term, $out);
    if (not $stdin) {
        $term = Term::ReadLine->new('Fumo Queue');
        $out = ($term->OUT or *STDOUT);
    }
    else {
        $out  = *STDOUT;
    }

    my %dispatch = (
        list   => \&list,
        ls     => \&list,

        add    => \&add,
        delete => \&delete,

        up     => \&up,
        down   => \&down,

        empty  => sub { shift->empty() }
    );

    while (defined ($_ = ($term) ? $term->readline('fumo> ') : <STDIN>)) {
        # split taking into consideration quoted strings
        my @args;
        while (m/"([^"\\]*(\\.[^"\\]*)*)"|([^\s+]+)/g) {
            push(@args, defined($1) ? $1 : $3)
        }
        my $command = shift @args;

        if (exists $dispatch{$command}) {
            eval { $dispatch{$command}->($queue, $out, @args) };
            if ($@) {
                print $out $@;
            }
        }
        else {
            print $out "Nothing know about '$command'.";
        }
    }
}
