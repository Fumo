use Test::More tests => 7;
use Test::Exception;

use strict;
use warnings;

use YAML;
use Fumo::Import;
use Data::Dumper;

use Cwd qw(realpath);
use File::Basename;

BEGIN { use_ok 'Fumo::Schema' }

SKIP: {
    skip('FUMO_TEST_DSN environment variable not set', 3)
        unless $ENV{FUMO_TEST_DSN};

    # the files
    my $file_map = {
        'one_file_all_combos_valid.yml' => {
            name     => 'one file, all combos, valid',
            throws   => undef,
            revision => 'Revision1',
        },
        'one_file_skip_todo.yml' => {
            name     => 'one file, todos and skips, valid',
            throws   => undef,
            revision => 'Revision2',
        },
        'one_file_invalid.yml' => {
            name     => 'one file, invalid (extra data)',
            throws   => 'forbidden field: thisisinvalid',
            revision => 'Revision3',
        },
        'many_files_same_tests.yml' => {
            name     => 'many files, same tests',
            throws   => undef,
            revision => 'Revision3',
        }
    };

    # grab the schema
    my $schema = Fumo::Schema->connect($ENV{FUMO_TEST_DSN});
    isa_ok($schema, 'Fumo::Schema');

    # clear the database
    my $projects_rs = $schema->resultset('Project');
    $projects_rs->delete_all();
    cmp_ok($projects_rs->count(), '==', 0,
	   'There are no projects in the database');

    my $importer = Fumo::Import->new();

    my $fixture_path = dirname(realpath($0)) . '/01-fixtures/';
    for my $file (keys %{ $file_map }) {
        my $details = YAML::LoadFile($fixture_path . $file);

        # the actual import
        my $import = sub {
            $importer->do_import($details, $schema,
                'Project1', 'Branch1', $file_map->{$file}->{revision})
        };
        
        # check it lives or throws as it should
        if ($file_map->{$file}->{throws}) {
 	    throws_ok { $import->() } qr/$file_map->{$file}->{throws}/,
                "'$file' throws ok";
        }
        else {
 	    lives_ok { $import->() }
                "'$file' imports ok" or diag($file_map->{$file}->{name});
        }
    }
}
