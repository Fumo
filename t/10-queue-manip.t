use Test::More tests => 31;
use Test::Exception;

use strict;
use warnings;

use DBI;

BEGIN {
    use_ok 'Fumo::Queue';
    use_ok 'Fumo::Queue::Run';
}

SKIP: {
    skip('FUMO_TEST_DSN environment variable not set', 17)
        unless $ENV{FUMO_TEST_DSN};

    # just bail if we can't connect
    my $dbh = DBI->connect($ENV{FUMO_TEST_DSN})
        or BAIL_OUT(DBI->errstr);

    # create the high level queue
    my $queue = Fumo::Queue->new($dbh);
    isa_ok($queue, 'Fumo::Queue');

    # remove all runs
    lives_ok { $queue->empty() } 'Delete all runs from the queue';
    cmp_ok $queue->count(), '==', 0, 'Queue count is 0';

    # create a new run
    my $run = Fumo::Queue::Run->new({
        project  => 'Project1',
        branch   => 'Branch1',
        revision => 'Revision1'
    });
    isa_ok($run, 'Fumo::Queue::Run');

    # add to the queue
    lives_ok { $queue->put_run($run) } 'Adding new run to queue';

    # so now there should be one run
    cmp_ok $queue->count(), '==', 1, 'Queue count is 1';

    $run = undef;

    # try and get it back again
    lives_ok { $run = $queue->get_run('Project1', 'Branch1', 'Revision1') }
        'Getting an existing branch lives';
    isa_ok($run, 'Fumo::Queue::Run');

    # does the run contain what it should?
    {
        my %hopefor = (
            project  => 'Project1',
            branch   => 'Branch1',
            revision => 'Revision1',
            weight   => 0,
        );

        for my $field (keys %hopefor) {
            no strict 'refs';

            cmp_ok($run->$field, 'eq', $hopefor{$field},
                   "'$field' is corrent for run ($hopefor{$field})");
        }
    }

    # try updating the run
    {
        $run->weight(110);

        # add to the queue
        lives_ok { $queue->put_run($run) } 'Adding new run to queue lives';

        # this will do another DB lookup
        my $copy_of_run = $queue->get_run('Project1', 'Branch1', 'Revision1');
        cmp_ok $copy_of_run->weight(), '==', 110,
            'New weight is 110';
    }

    # try and add the same run (it will actually do an update)
    {
        lives_ok { $queue->put_run($run) }
            'Adding identical run to queue lives';
    }

    # delete the run
    {
        lives_ok { $queue->delete($run) } 'Deleting a run lives'; 
        cmp_ok $queue->count(), '==', 0, 'Queue count is 0 again';       
    }

    # loop all runs
    {
        for (1 .. 5) {
            lives_ok {
                $queue->put_run(Fumo::Queue::Run->new({
                    project  => "project$_",
                    branch   => "branch1$_",
                    revision => "revision$_"
                }))
            } "Adding new run ($_) to queue";

            # count the entries
            my $ctr = 0;
            while (my $incr = $queue->all_runs()) {
                $ctr++;
            }
            cmp_ok $ctr, '==', $_, "all_runs returns $_ results";
        }
    }

    # delete the runs
    {
        lives_ok { $queue->delete($_) while $_ = $queue->all_runs() }
            'Deleting all runs with all_runs() lives ok';
        cmp_ok $queue->count(), '==', 0, 'Queue count is 0 again';       
    }
}
