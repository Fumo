use strict;
use warnings;
use Test::More tests => 7;

BEGIN { use_ok 'Catalyst::Test', 'Fumo' }

my %expected = (
    '/'                                   => [ '200' ],
    '/project/'                           => [ '200' ],
    '/project/Project1'                   => [ '200' ],
    '/project/Project1/Branch1'           => [ '200' ],
    '/project/Project1/Branch1/Revision1' => [ '200' ],

    '/thisisrubbish'                      => [ '404' ],
);

for (keys %expected) {
    my ($status) = @{ $expected{$_} };

    cmp_ok( request($_)->code(), 'eq', $status,
            "Fetching '$_' returns a '$status'");
}
