use Test::More 'no_plan';
use Test::Exception;

use strict;
use warnings;

BEGIN { use_ok 'Fumo::Schema' }

SKIP: {
    skip('FUMO_TEST_DSN environment variable not set', 3)
        unless $ENV{FUMO_TEST_DSN};

    # grab the schema
    my $schema = Fumo::Schema->connect($ENV{FUMO_TEST_DSN});
    isa_ok($schema, 'Fumo::Schema');

    my $revision_rs = $schema->resultset('Revision')->find({
        project => 'Project1',
        branch  => 'Branch1',
        name    => 'Revision3'
    });

    # should be 50%
    cmp_ok($revision_rs->pass_rate(), '==', 50, 'passrate is 50%');
}
