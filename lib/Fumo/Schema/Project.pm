package Fumo::Schema::Project;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("project");
__PACKAGE__->add_columns(
  "name",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "created",
  { data_type => "DATETIME", default_value => "", is_nullable => 0, size => 19 },
);
__PACKAGE__->set_primary_key("name");
__PACKAGE__->has_many(
  "branches",
  "Fumo::Schema::Branch",
  { "foreign.project" => "self.name" },
);
__PACKAGE__->has_many(
  "files",
  "Fumo::Schema::File",
  { "foreign.project" => "self.name" },
);
__PACKAGE__->has_many(
  "revisions",
  "Fumo::Schema::Revision",
  { "foreign.project" => "self.name" },
);
__PACKAGE__->has_many(
  "tests",
  "Fumo::Schema::Test",
  { "foreign.project" => "self.name" },
);


# Created by DBIx::Class::Schema::Loader v0.04001 @ 2007-07-20 22:25:16
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:RjW8qQZQE2davv6DkZFGSQ

use DateTime;

sub insert {
    my $self = shift;
    $self->created(DateTime->now);

    return $self->next::method(@_);
}

1;
