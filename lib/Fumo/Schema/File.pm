package Fumo::Schema::File;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("file");
__PACKAGE__->add_columns(
  "name",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "project",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "branch",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "revision",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "test_count",
  { data_type => "INT", default_value => "", is_nullable => 0, size => 11 },
  "created",
  { data_type => "DATETIME", default_value => "", is_nullable => 0, size => 19 },
  "pass_rate",
  { data_type => "FLOAT", default_value => 0, is_nullable => 0, size => 32 },
);
__PACKAGE__->set_primary_key("name", "project", "branch", "revision");
__PACKAGE__->belongs_to("branch", "Fumo::Schema::Branch", { name => "branch" });
__PACKAGE__->belongs_to("project", "Fumo::Schema::Project", { name => "project" });
__PACKAGE__->belongs_to("revision", "Fumo::Schema::Revision", { name => "revision" });
__PACKAGE__->has_many(
  "tests",
  "Fumo::Schema::Test",
  { "foreign.file" => "self.name" },
);


# Created by DBIx::Class::Schema::Loader v0.04001 @ 2007-07-20 22:25:16
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:47+jRPE2lQskHivy9bN5hw

use DateTime;

sub insert {
    my $self = shift;

    $self->created(DateTime->now);

    return $self->next::method(@_);
}

sub pass_rates {
    my ($self, $project, $branch, $revision) = @_;

    $self->search_related('tests', 
        { directive => undef },
        {
            select   => [ 'status', { count => 'status' } ],
            as       => [ 'status', 'status_count' ],
            group_by => 'status'
        }
    );
}

sub tests_by_status {
    my ($self, $status, $project, $branch, $revision) = @_;

    # only allow the valid test types through
    my $search = {
	project  => $project,
	branch   => $branch,
	revision => $revision
    };

    if ($status) {
	$search->{status} = $status;
        $search->{directive} = undef;
    }

    return $self->search_related('tests', $search, {
        order_by => 'number'
    });
}

1;
