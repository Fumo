package Fumo::Schema::Test;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("test");
__PACKAGE__->add_columns(
  "name",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "project",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "branch",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "revision",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "file",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "number",
  { data_type => "INT", default_value => "", is_nullable => 0, size => 11 },
  "status",
  { data_type => "ENUM", default_value => undef, is_nullable => 1, size => 6 },
  "created",
  { data_type => "DATETIME", default_value => "", is_nullable => 0, size => 19 },
  "directive",
  { data_type => "ENUM", default_value => undef, is_nullable => 1, size => 4 },
  "pass_rate",
  { data_type => "INT", default_value => "", is_nullable => 0, size => 3 },
);
__PACKAGE__->set_primary_key("name", "project", "branch", "revision", "file");
__PACKAGE__->belongs_to("branch", "Fumo::Schema::Branch", { name => "branch" });
__PACKAGE__->belongs_to("file", "Fumo::Schema::File", { name => "file" });
__PACKAGE__->belongs_to("project", "Fumo::Schema::Project", { name => "project" });
__PACKAGE__->belongs_to("revision", "Fumo::Schema::Revision", { name => "revision" });


# Created by DBIx::Class::Schema::Loader v0.04001 @ 2007-07-20 22:25:16
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:FfzfkNApD4CI9ESybl2zGg

use DateTime;

sub insert {
    my $self = shift;
    $self->created(DateTime->now);

    return $self->next::method(@_);
}

1;
