package Fumo::Schema::Revision;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("revision");
__PACKAGE__->add_columns(
  "name",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "project",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "branch",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "created",
  { data_type => "DATETIME", default_value => "", is_nullable => 0, size => 19 },
  "pass_rate",
  { data_type => "INT", default_value => 0, is_nullable => 0, size => 3 },
);
__PACKAGE__->set_primary_key("name", "project", "branch");
__PACKAGE__->has_many(
  "files",
  "Fumo::Schema::File",
  { "foreign.revision" => "self.name" },
);
__PACKAGE__->belongs_to("branch", "Fumo::Schema::Branch", { name => "branch" });
__PACKAGE__->belongs_to("project", "Fumo::Schema::Project", { name => "project" });
__PACKAGE__->has_many(
  "tests",
  "Fumo::Schema::Test",
  { "foreign.revision" => "self.name" },
);


# Created by DBIx::Class::Schema::Loader v0.04001 @ 2007-07-20 22:25:16
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:G56rSJtt6DLqc9xNTIoQGA

use DateTime;

sub insert {
    my $self = shift;
    $self->created(DateTime->now);

    return $self->next::method(@_);
}

1;
