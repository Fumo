package Fumo::Schema::Branch;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("Core");
__PACKAGE__->table("branch");
__PACKAGE__->add_columns(
  "name",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "project",
  { data_type => "VARCHAR", default_value => "", is_nullable => 0, size => 128 },
  "created",
  { data_type => "DATETIME", default_value => "", is_nullable => 0, size => 19 },
);
__PACKAGE__->set_primary_key("name", "project");
__PACKAGE__->belongs_to("project", "Fumo::Schema::Project", { name => "project" });
__PACKAGE__->has_many(
  "files",
  "Fumo::Schema::File",
  { "foreign.branch" => "self.name" },
);
__PACKAGE__->has_many(
  "revisions",
  "Fumo::Schema::Revision",
  { "foreign.branch" => "self.name" },
);
__PACKAGE__->has_many(
  "tests",
  "Fumo::Schema::Test",
  { "foreign.branch" => "self.name" },
);


# Created by DBIx::Class::Schema::Loader v0.04001 @ 2007-07-20 22:25:16
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:rdJHIvFMKESw/UfGvklyfg

use DateTime;

sub insert {
    my $self = shift;
    $self->created(DateTime->now);

    return $self->next::method(@_);
}

1;

