package Fumo::Queue;

use Carp;

=head1 NAME

Fumo::Queue - Manipulate a queue of smoke server runs.

=head1 DESCRIPTION

Queue holds a queue of test runs that are to be run. It allows
manipulation of a weight field so that users can change which runs
happen when.

=head1 METHODS

=head2 new

=over 4

=item Arguments: $dbh

=item Return Value: $self

=back

Initialises a new Fumo::Queue object.

=cut

sub new {
    my ($class, $dbh) = @_;
    my $self = {
        dbh     => $dbh,
        all_sth => undef
    };

    bless($self, $class);

    return $self;
}

=head2 empty

=over 4

=item Arguments: none

=item Return Value: none

=back

Empties the queue completely. Will croak on failure.

=cut

sub all_runs {
    my $self = shift;

    if (not defined $self->{all_sth}) {
        $self->{all_sth} = $self->{dbh}->prepare(qq(
            SELECT
                *
            FROM
                queue
            ORDER BY
                weight DESC
        ));
    
        $self->{all_sth}->execute()
            or croak $self->{dbh}->errstr();
    }

    my $details = $self->{all_sth}->fetchrow_hashref();

    if (defined $details) {
        return Fumo::Queue::Run->new($details)
    }
    else {
        return $self->{all_sth} = undef;
    }
}

=head2 empty

=over 4

=item Arguments: none

=item Return Value: none

=back

Empties the queue completely. Will croak on failure.

=cut

sub empty {
    my $self = shift;

    $self->{dbh}->do(q(
        DELETE
        FROM
            queue
    )) or croak $self->{dbh}->errstr();
}

=head2 count

=over 4

=item Arguments: none

=item Return Value: Queue count

=back

Returns the number of runs in the queue. Croaks should there be a problem.

=cut

sub count {
    my $self = shift;

    my $sth = $self->{dbh}->prepare(q(
        SELECT
            count(project) as count
        FROM
            queue
    )) or croak $self->{dbh}->errstr();

    $sth->execute()
        or croak $self->{dbh}->errstr();

    return $sth->fetchrow_hashref->{count};
}

=head2 put_run

=over 4

=item Arguments: Queue::Run object

=item Return Value: none

=back

Add a Queue::Run object to the queue.

=cut

sub put_run {
    my ($self, $run) = @_;

    # does it this run already exist?
    if (not $self->get_run($run->project, $run->branch, $run->revision)) {
        my $sth = $self->{dbh}->prepare(q(
            INSERT INTO
                queue (project, branch, revision, created)
            VALUES
                (?, ?, ?, now())
        )) or croak $self->{dbh}->errstr;

        $sth->execute($run->project, $run->branch, $run->revision)
            or croak $self->{dbh}->errstr;
    }
    else {
        my $sth = $self->{dbh}->prepare(q(
            UPDATE
               queue
            SET
               created = ?, run_start = ?, weight = ?, status = ?, comment = ?
            WHERE
               project = ? AND branch = ? AND revision = ?
        ));

        # grab the argument from the Fumo::Queue::Run
        my @args;
        {
            no strict 'refs';

            @args = map {
                $run->$_
            } qw(created run_start weight status comment project branch
                 revision);
        }

        $sth->execute(@args)
            or croak $self->{dbh}->errstr;
    }
}

=head2 delete

=over 4

=item Arguments: Queue::Run

=item Return Value: none

=back

Delete the Queue::Run object given (it must have at least project,
branch and revision attributes).

=cut

sub delete {
    my ($self, $run) = @_;

    my $sth = $self->{dbh}->prepare(q(
        DELETE
        FROM
            queue
        WHERE
            project = ? AND branch = ? AND revision = ?
    )) or croak $self->{dbh}->errstr();

    $sth->execute($run->project, $run->branch, $run->revision)
        or croak $self->{dbh}->errstr;
}

=head2 get_run

=over 4

=item Arguments: $project, $branch, $revision

=item Return Value: Queue::Run

=back

Grab the Queue::Run object with the project, branch and revision.

=cut

sub get_run {
    my ($self, $project, $branch, $revision) = @_;

    my $sth = $self->{dbh}->prepare(q(
        SELECT
            *
        FROM
            queue
        WHERE
            project = ? AND branch = ? AND revision = ?
    )) or croak $self->{dbh}->errstr;

    $sth->execute($project, $branch, $revision)
        or croak $self->{dbh}->errstr;

    my $details = $sth->fetchrow_hashref();

    return (defined $details)
        ? Fumo::Queue::Run->new($details)
        : undef;
}

return 1;
