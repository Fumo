package Fumo::Import::DataSchema;

use Data::Dumper;
use Data::Domain qw(:all);

use strict;
use warnings;

=head1 NAME

DataSchema - Access to a schema.

=head1 SYNOPSIS

 use Fumo::Import::DataSchema qw(data_schema);
 use Data::Domain;

 my $domain = data_schema();
 $domain->inspect($data);

=head1 DESCRIPTION

Literally returns just a data structure, nothing fancy.

=head1 METHODS

=cut

require Exporter;

our @ISA = qw(Exporter);
our @EXPORT_OK = qw(data_schema);

=head2 _tests

=over 4

=item Arguments: $status_values

=item Return Value: List

=back

Data::Domain object representing a tests list.

=cut

sub _tests {
    my ($status_values, $directive_values) = @_;

    return List(
        -all => Struct(
            -fields => {
                number    => Int(),
                name      => String( -length => [ 1, 255 ] ),
                status    => Enum(@{$status_values}),
		directive => Enum(@{$directive_values}),
            },
            -exclude => '*'
        )
    );
}

=head2 _files

=over 4

=item Arguments: $status_values

=item Return Value: 

=back

Data::Domain object representing a files list.

=cut

sub _files {
    my ($status_values, $directive_values) = @_;

    return List(
        -all => Struct(
            -fields => {
                name       => String( -length => [ 1, 255 ] ),
                tests      => _tests($status_values, $directive_values),
                test_count => Whatever()              # TODO: undef or Int()
            },
            -exclude => '*'
        )
    );
}

=head2 data_schema

=over 4

=item Arguments: $status_values

=item Return Value: Struct

=back

Data::Domain object representing an entire Fumo entry.

=cut

sub data_schema {
    my ($status_values, $directive_values) = @_;

    return Struct(
        -fields => {
            files => _files($status_values, $directive_values)
        },
        -exclude => '*'
    );
}

return 1;
