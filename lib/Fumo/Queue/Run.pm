package Fumo::Queue::Run;

use base qw(Class::Accessor);

Fumo::Queue::Run->mk_accessors(qw(
    project
    revision
    branch
    weight
    created
    run_start
    status
    comment
));

return 1;
