package Fumo::Import;

use strict;
use warnings;

use Data::Dumper;
use Fumo::Import::DataSchema qw(data_schema);

=head1 NAME

Fumo::Controller::Revision - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller representing a revision (changeset).

=head1 METHODS

=head2 new

Initialises a new Fumo::Import object.

=cut

sub new {
    my ($class) = @_;
    my $self = { };

    bless($self, $class);

    return $self;
}

=head2 do_import

=over 4

=item Arguments: $data, $schema, $project_name, $branch_name, $revision_name

=item Return Value: $schema

=back

This method will import $data into the context of the *_name arguments.

=cut

sub do_import {
    my ($self, $data, $schema, $project_name, $branch_name,
	$revision_name) = @_;

    # is the data valid?
    $self->_validate($data);

    $schema->txn_do(
        sub {
            my $branch_rs = $schema->resultset('Project')->find_or_create(
                name => $project_name
            )->branches->find_or_create(
                name => $branch_name,
                project => $project_name
            );
    
            my $revision_rs = $branch_rs->revisions->find_or_create(
                name => $revision_name,
                project => $project_name,
                branch => $branch_name
            );

            #$revision_rs->files->delete_all;

            # these are stored for the revision pass_rate
            my ($total_tests_passed, $total_test_count);

            # import the files
            for my $file (@{$data->{files}}) {
                my ($file_tests_passed, $file_test_count);

                # zero count to avoid DBIC error for not-null column
                $file_test_count = 0;

                my $file_rs = $revision_rs->files->find_or_create({
                    name => $file->{name},
                    project => $project_name,
                    branch => $branch_name,
                    test_count => $file_test_count,
                });

                # import the tests within a file
                for my $test (@{$file->{tests}}) {
                    $file_test_count++;

                    if ($test->{directive} or $test->{status} eq 'ok') {
                        $file_tests_passed++;
                    }

                    my $test_rs = $file_rs->tests->find_or_create({
                        name => $test->{name},
                        number => $test->{number},
                        project => $project_name,
                        branch => $branch_name,
                        revision => $revision_name,
                        status => $test->{status},
                        directive => ($test->{directive} or undef)
                    });
                }

		$total_tests_passed += $file_tests_passed;
		$total_test_count   += $file_test_count;

                # update the file level pass rate
                if ($file_tests_passed) {
                    my $passed = ($file_tests_passed * 100) / $file_test_count;
                    $file_rs->pass_rate($passed);

                    $file_rs->update();
                }
            }

            if ($total_tests_passed) {
                my $passed = ($total_tests_passed * 100) / $total_test_count;
                $revision_rs->pass_rate($passed);

                $revision_rs->update();
            }
        }
    );

    return $schema;
}

=head2 _validate

=over 4

=item Arguments: $data

=item Return Value: none

=back

This method will validate the data passed in as $data and throw an
exception of the errors if any.

=cut

sub _validate {
    my ($self, $data) = @_;

    my @status    = ('ok', 'not ok');
    my @directive = ('SKIP', 'TODO', '');

    my $domain = data_schema(\@status, \@directive);

    my $errors = $domain->inspect($data);
    die Dumper $errors if $errors;
}

1;
