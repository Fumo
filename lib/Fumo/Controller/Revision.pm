package Fumo::Controller::Revision;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Data::Dumper;

=head1 NAME

Fumo::Controller::Revision - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller representing a revision (changeset).

=head1 METHODS

=head2 revision_list

Lists the revisions avalible to the user considering the
/project/branch route so far.

=cut

sub revision_list : Chained('/branch/branch_instance')
                    PathPart('') Args(0) {
    my ($self, $c) = @_;

    $c->stash->{revisions} = [
        $c->model('Schema::Revision')->search( $c->stash->{search_stack} )
    ];

    $c->stash->{template} = 'revision/revision_list.tt2';
}

=head2 revision_instance

Passes on the current revision name within the chain.

=cut

sub revision_instance : Chained('/branch/branch_instance')
                        PathPart('') CaptureArgs(1) {
    my ($self, $c, $revision) = @_;

    $c->stash->{search_stack}->{revision} = $revision;
}

=head1 AUTHOR

Phil Jackson, phil@shellarchive.co.uk

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
