package Fumo::Controller::Project;

use strict;
use warnings;

use base 'Catalyst::Controller';

use Data::Dumper;

=head1 NAME

Fumo::Controller::Project - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller which represents a project.

=head1 METHODS

=cut

=head2 project_list

Lists the projects avalible to the user and pass them on to the
template.

=cut

sub project_list : Chained('/') PathPart('project') Args(0) {
    my ($self, $c) = @_;

    $c->stash->{uri_path} = '/' . $c->request->{path};
    $c->stash->{uri_path} =~ s#/*(.+?)/*$#/$1/#;

    $c->stash->{projects} = [ $c->model('Schema::Project')->all ];
    $c->stash->{template} = 'project/project_list.tt2';
}

=head2 file_instance

Passes on the current file name within the chain.

=cut

sub project_instance : Chained('/') PathPart('project') CaptureArgs(1) {
    my ($self, $c, $project) = @_;

    $c->stash->{uri_path} = '/' . $c->request->{path};
    $c->stash->{uri_path} =~ s#/*(.+?)/*$#/$1/#;

    $c->stash->{search_stack}->{project} = $project;
}

#sub instance : Chained('/') 

=head1 AUTHOR

Phil Jackson, phil@shellarchive.co.uk

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
