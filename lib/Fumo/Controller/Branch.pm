package Fumo::Controller::Branch;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Data::Dumper;

=head1 NAME

Fumo::Controller::Branch - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller representing a branch.

=head1 METHODS

=cut

=head2 branch_list

Lists the branches avalible to the user considering the
/project  route so far.

=cut

sub branch_list : Chained('/project/project_instance')
                  PathPart('') Args(0) {
    my ($self, $c) = @_;

    $c->stash->{branches} = [
        $c->model('Schema::Branch')->search( $c->stash->{search_stack} )
    ];

    $c->stash->{template} = 'branch/branch_list.tt2';
}

=head2 branch_instance

Passes on the current branch name within the chain.

=cut

sub branch_instance : Chained('/project/project_instance')
                      PathPart('') CaptureArgs(1) {
    my ($self, $c, $branch) = @_;

    $c->stash->{search_stack}->{branch} = $branch;
}

=head1 AUTHOR

Phil Jackson, phil@shellarchive.co.uk

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
