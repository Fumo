package Fumo::Controller::Root;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Data::Dumper;

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config->{namespace} = '';

=head1 NAME

Fumo::Controller::Root - Root Controller for Fumo

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=cut

sub index : Private {
    my ($self, $c) = @_;

    $c->stash->{template} = 'start.tt2';
}

=head2 default

=cut

sub default : Private {
    my ($self, $c) = @_;

     $c->res->status(404);
     $c->stash->{template} = '404.tt2';
}

=head2 end

Attempt to render a view, if needed.

=cut 

sub end : ActionClass('RenderView') {
    my ($self, $c) = @_;

}

=head1 AUTHOR

Philip Jackson,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
