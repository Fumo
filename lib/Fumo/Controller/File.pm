package Fumo::Controller::File;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Fumo::Import;
use Data::Dumper;
use YAML;

=head1 NAME

Fumo::Controller::File - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller which represents a file (usually a .t).

=head1 METHODS

=cut

=head2 file_list

Lists the files avalible to the user considering the
/project/branch/revision route so far.

=cut

sub file_list : Chained('/revision/revision_instance')
                PathPart('') Args(0) {
   my ($self, $c) = @_;

   if ($c->request->method() eq 'POST') {
       if ($c->request->content_type() eq 'text/x-yaml') {
	   my $import_result = $self->import_details($c);

	   $c->response->body($import_result);
       }
       else {
	   # TODO: what is the unsupported method status?
	   $c->response->status('402');
       }
   }
   else {
       $c->stash->{files} = [
	   $c->model('Schema::File')->search( $c->stash->{search_stack} )
       ];

       $c->stash->{status}   = $c->request->param('status');
       $c->stash->{template} = 'file/file_list.tt2';
   }
}

=head2 file_instance

Passes on the current file name within the chain.

=cut

sub file_instance : Chained('/revision/revision_instance')
                    PathPart('') CaptureArgs(1) {
    my ($self, $c, $file) = @_;
    
    $c->stash->{search_stack}->{file} = $file;
}

=head2 import_details

When a POST is encountered by file_list this method will be
executed.

=cut

sub import_details : Private {
    my ($self, $c) = @_;

    my $importer = Fumo::Import->new();
    my $content = $c->request->body();

    # slurp in the entire file
    {
	local $/ = undef;
	$content = <$content>;
    }

    # serialize and import
    my $serialized = YAML::Load($content);
    $importer->do_import($serialized, $c->model('Schema')->schema(),
        map { $c->stash->{search_stack}->{$_} } qw(project branch revision));
}

=head2 end

Errors from a post should be in plain text(?) At the moment just dump
errors in YAML and return a 400.

=cut 

sub end : ActionClass('RenderView') {
    my ($self, $c) = @_;

    if (scalar @{$c->error} and $c->request->method() eq 'POST') {
	$c->response->content_type('text/x-yaml');

	$c->response->status('400');
	$c->response->body(Dumper $c->error());

	$c->error(0);
    }
}

=head1 AUTHOR

Phil Jackson, phil@shellarchive.co.uk

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
