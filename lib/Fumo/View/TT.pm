package Fumo::View::TT;

use strict;
use base 'Catalyst::View::TT';

__PACKAGE__->config({
    CATALYST_VAR => 'Catalyst',
    INCLUDE_PATH => [
        Fumo->path_to( 'root', 'src' ),
        Fumo->path_to( 'root', 'lib' )
    ],
    WRAPPER      => 'global.tt2',
    ERROR        => 'error.tt2',
    TIMER        => 0,
    DEBUG        => undef
});

sub new {
    my $class = shift;
    my $self = $class->NEXT::new(@_);

    $self->{template}->context->define_filter(
        firstchar => sub {
            my ($expr) = @_;

            return substr($expr, 0, 1);
        }
    );

    return $self;
}

=head1 NAME

Fumo::View::TT - Catalyst TTSite View

=head1 SYNOPSIS

See L<Fumo>

=head1 DESCRIPTION

Catalyst TTSite View.

=head1 AUTHOR

Philip Jackson,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;

