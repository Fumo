package Fumo::Model::Schema;

use strict;
use base 'Catalyst::Model::DBIC::Schema';

=head1 NAME

Fumo::Model::Schema - Catalyst DBIC Schema Model
=head1 SYNOPSIS

See L<Fumo>

=head1 DESCRIPTION

L<Catalyst::Model::DBIC::Schema> Model using schema L<Fumo::Schema>

=head1 AUTHOR

Philip Jackson,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
